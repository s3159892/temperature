package nl.utwente.di.bookQuote;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class  BookQuote extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Quoter quoter;

    public void init() throws ServletException {
        quoter = new Quoter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Celsius to Fahrenheit Converter";
        double cel = 0.0;
        double far = 0.0;
        String celStr = request.getParameter("celsius");
        if(celStr != null && !celStr.isEmpty()){
            cel = Double.parseDouble(celStr);
            far = quoter.getC2F(cel);
        }

        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                            "<H1>" + title + "</H1>\n" +
                "<P>Celsius: " + cel + "</P>\n" +
                "<p>Fahrenheit: " + far + "</P>\n" +
                "</BODY>\n" +
                "</HTML>");

    }
}
