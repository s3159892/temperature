package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class testTemp{
    @Test
    public void testTemp() throws Exception {
        Quoter quoter = new Quoter();
        double fahrenheit = quoter.getC2F(30.0);
        Assertions.assertEquals(86.0, fahrenheit , 0.0);
    }
}
